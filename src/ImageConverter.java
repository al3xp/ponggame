import javax.imageio.ImageIO;
import javax.imageio.ImageReadParam;
import javax.imageio.ImageReader;
import javax.imageio.stream.ImageInputStream;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;


public class ImageConverter {

	public static byte[] getBytesFromImage(BufferedImage image) throws IOException {
		byte[] bytearray = new byte[0];
		if (image != null) {
			ByteArrayOutputStream baos = new ByteArrayOutputStream(1000);
			ImageIO.write(image, "jpg", baos);
			baos.flush();
			bytearray = baos.toByteArray();
			baos.close();
		}
		return bytearray;
	}


	public static BufferedImage getImageFromBytes(byte[] bytes) throws IOException {
		BufferedImage img = null;
		if (bytes.length > 0) {
			img = ImageIO.read(new ByteArrayInputStream(bytes));
		}
		return img;
	}

}
