import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

public class SpectatorListener {

    private ObjectInputStream in;
    private Game game;

    private boolean threadsActive = true;

    public SpectatorListener(String serverIP, int port, int imagePort) {
        Socket socket;
        while(true) {
            try {
                socket = new Socket(serverIP, port);
                break;
            } catch (IOException e) {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e2) {
                    e2.printStackTrace();
                }
            }
        }

        try {
            in = new ObjectInputStream(socket.getInputStream());
        } catch (IOException e) {
            e.printStackTrace();
        }

        Thread t = new Thread(() -> {
            while(threadsActive) {
                listen();
            }
        });
        t.start();
    }

    private void listen() {
        try {
            Object data;
            while((data = in.readObject()) != null) {
                if(data.getClass() == Player.class) {
                    Player player = (Player)data;
                    if(player.isHost()) {
                        game.setHostPlayer(player);
                    } else {
                        game.setClientPlayer(player);
                    }
                } else if (data.getClass() == Pong.class) {
                    game.setPong((Pong)data);
                }
                if(!threadsActive) {
                    break;
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    public void setGame(Game game) {
        this.game = game;
    }

    public void stopListener() {
        threadsActive = false;
    }
}
