import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.*;
import java.util.ArrayList;
import java.util.ConcurrentModificationException;

public class Server {

	private boolean socketBeforeConnected = true;

	private ServerSocket server;
	private DatagramSocket imageServer;
	private Game game;
	private ArrayList<ObjectOutputStream> outs = new ArrayList<>();

	private boolean threadsActive = true;

	final static int MOVEMENT_UP = 0;
	final static int MOVEMENT_DOWN = 1;
	final static int MOVEMENT_STOP = 2;

	private ObjectOutputStream clientOut;

	public Server(int port, int imagePort) {
		try {
			server = new ServerSocket(port);
			imageServer = new DatagramSocket(imagePort);
			System.out.println(server);
			System.out.println(imageServer);
		} catch (IOException e) {
			e.printStackTrace();
		}
		Thread socketConnector = new Thread(() -> {
			while (threadsActive) {
				try {
					Thread.sleep(100);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				if (socketBeforeConnected) {
					socketBeforeConnected = false;
					Thread t = new Thread(() -> {
						while (threadsActive) {
							listen();

						}
					});
					t.start();
					Thread t2 = new Thread(() -> {
						listenImage();
					});
					t2.start();
				}
			}
		});
		socketConnector.start();
	}

	private void listen() {
		try {
			Socket socket = server.accept();
			socketBeforeConnected = true;
			System.out.println("Client connected: " + socket.getInetAddress());
			ObjectOutputStream out = new ObjectOutputStream(socket.getOutputStream());
			outs.add(out);
			game.sendReady = true;
			ObjectInputStream in = new ObjectInputStream(socket.getInputStream());
			Object data;
			while((data = in.readObject()) != null) {
				if(data.getClass() == Player.class) {
					Player player = (Player)data;
					if(player.isHost()) {
						game.setHostPlayer(player);
					} else {
						if(game.clientPlayer == null) {
							clientOut = out;
							game.setClientPlayer(player);
						} else {
							outs.remove(out);
							out.writeObject("ClientAlreadyConnected");
							out.flush();
						}
					}
				} else if (data.getClass() == Pong.class) {
					game.setPong((Pong)data);
				} else if (data.getClass() == Integer.class) {
					int movementStatus = (int) data;
					if(movementStatus == MOVEMENT_DOWN) {
						game.clientPlayer.speedY = game.optionsPlayerSpeed;
					} else if(movementStatus == MOVEMENT_UP){
						game.clientPlayer.speedY = -game.optionsPlayerSpeed;
					} else if(movementStatus == MOVEMENT_STOP){
						game.clientPlayer.speedY = 0;
					}
				}  else if (data.getClass() == byte[].class) {
					game.setImage((byte[]) data);
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
	}

	public void listenImage(){
		while(true)
		{
			byte[] sendData = game.getImage();
			byte[] receiveData = new byte[100000];

			DatagramPacket receivePacket = new DatagramPacket(receiveData, receiveData.length);
			try {
				imageServer.receive(receivePacket);
			} catch (IOException e) {
				e.printStackTrace();
			}
			game.setImage(receivePacket.getData());

			InetAddress IPAddress = receivePacket.getAddress();
			int port = receivePacket.getPort();

			DatagramPacket sendPacket =
					new DatagramPacket(sendData, sendData.length, IPAddress, port);
			try {
				imageServer.send(sendPacket);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	public void sendObject(Object object) {
		try {
			for(ObjectOutputStream out : outs) {
				try {
					out.writeObject(object);
					out.flush();
					out.reset();
				} catch (IOException e) {
					outs.remove(out);
					if(out.equals(clientOut)) {
						clientOut = null;
					}
					e.printStackTrace();
				}
			}
		} catch (ConcurrentModificationException e) {
			e.printStackTrace();
		}
	}
	
	public void setGame(Game game) {
		this.game = game;
	}

	public void stopThreads() {
		threadsActive = false;
	}
}
