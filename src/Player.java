import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.net.*;

public class Player implements KeyListener, Serializable{
    final static long serialVersionUID = 1;
	
	private boolean isHost;

    public int x,y,w,h,speed=10,speedY, score;

    
    private Color color = Color.WHITE;
	private int responseTime = 5;
	private long lastTimestamp = System.currentTimeMillis();
	double pongTarget;

	public Player(boolean isHost, int x, int y, int w, int h) {
        this.isHost = isHost;
        this.x = x;
        this.y = y;
        this.w = w;
        this.h = h;
    }

    public void draw(Graphics g) {
    	g.setColor(color);
    	g.fillRect(x,y,w,h);
    }

    public boolean isHost() {
        return isHost;
    }

	@Override
	public void keyTyped(KeyEvent e) {}

	@Override
	public void keyPressed(KeyEvent e) {
		switch(e.getKeyCode()) {
		case KeyEvent.VK_UP:
			speedY = -speed;
			break;
		case KeyEvent.VK_DOWN:
			speedY = speed;
			break;
		}
	}

	@Override
	public void keyReleased(KeyEvent e) {
		switch(e.getKeyCode()) {
			case KeyEvent.VK_UP:
			case KeyEvent.VK_DOWN:
				speedY = 0;
				break;
		}
	}

	public void update() {
    	y += speedY;
    	if(y < 0 || y + h > Game.h) {
    		y -= speedY;
		}
	}

	public void recalculateMovement(Game game) {
		if (lastTimestamp + responseTime <= System.currentTimeMillis()) {
			pongTarget = game.pong.y;
			lastTimestamp = System.currentTimeMillis();
		} else {
			pongTarget = game.getHeight() / 2;
		}

		if (game.pong.vx < 0) {
			speedY = 0;
		}

    	if (game.pong.vx > 0) {
			if (y > pongTarget) {
				speedY = -speed;
			} else if (y < game.pong.y) {
				speedY = speed;
			} else {
				speedY = 0;
			}
		}
	}
}
