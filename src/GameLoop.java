import javax.imageio.ImageIO;

public class GameLoop extends Thread {

	private boolean isThreadRunning = true;
	private int frameRate = 30;
	private int imageFrameRate = 30;

	private Game game;

	public GameLoop(Game game) {
		this.game = game;
	}

	@Override
	public void run() {
		if(game.dataSender != null) {
			game.dataSender.sendObject(game.clientPlayer);
		}
		int counter = 0;
		while (isThreadRunning) {
			try {
				game.repaint();
				if (game.server != null && game.sendReady && game.clientPlayer != null) {
					game.update();
					game.server.sendObject(game.hostPlayer);
					game.server.sendObject(game.clientPlayer);
					game.server.sendObject(game.pong);

					if (game.isKiPlayer) {
						game.clientPlayer.recalculateMovement(game);
					}
				}
				Thread.sleep(1000 / frameRate);
			} catch (InterruptedException ex) {
				ex.printStackTrace();
			}

		}
	}

	private void sendImage() {
		if (game.server != null) {
			byte[] image = game.getImage();
			game.server.sendObject(image);
		}
		if (game.dataSender != null) {
			byte[] image = game.getImage();
			game.dataSender.sendObject(image);
		}
	}

	public boolean isThreadRunning() {
		return isThreadRunning;
	}
 
	public void stopGameLoop() {
		isThreadRunning = false;
	}
}