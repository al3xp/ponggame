import com.github.sarxos.webcam.Webcam;
import com.github.sarxos.webcam.WebcamResolution;
import enums.Direction;

import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;

public class Game extends JPanel {
	public Player hostPlayer;
	public Player clientPlayer;
	public Pong pong;
	
	public static final int w = 800;
	public static final int h = 500;

	private Color backgroundColor = Color.BLACK;
	private Color foregroundColor = Color.WHITE;
	
	private GameLoop gameLoop;
	
	public DataSender dataSender;
	public Server server;
	public SpectatorListener spectatorListener;
	
	public boolean sendReady = false;
	public boolean isKiPlayer;

	public int optionsPlayerSpeed;
	public int optionsPongSpeed;

	private SelectionWindow selectionWindow;

	private GameFrame gameFrame;
	private Webcam webcam;

	private BufferedImage imageFrame;
	private BufferedImage lastImageFrame;

	public Game(Server server, boolean isKiPlayer, SelectionWindow selectionWindow, int optionsPlayerSpeed, int optionsPongSpeed) {
		this.isKiPlayer = isKiPlayer;
		this.server = server;
		this.server.setGame(this);
		this.selectionWindow = selectionWindow;
		this.optionsPlayerSpeed = optionsPlayerSpeed;
		this.optionsPongSpeed = optionsPongSpeed;
		hostPlayer = new Player(true, 20, h/2-25, 20, 50);
		hostPlayer.speed = optionsPlayerSpeed;
		pong = new Pong(w/2,h/2, 6, optionsPongSpeed);
		webcam = createWebcam();
		setPreferredSize(new Dimension(w,h));
		gameFrame = new GameFrame(this);
		gameFrame.addKeyListener(hostPlayer);
		startGameLoop();
	}
	
	public Game(DataSender dataSender, boolean isKiPlayer, SelectionWindow selectionWindow) {
		this.isKiPlayer = isKiPlayer;
		this.dataSender = dataSender;
		this.dataSender.setGame(this);
		this.selectionWindow = selectionWindow;
		clientPlayer = new Player(false, w-40, h/2-25, 20, 50);

		if (!isKiPlayer) {
		    webcam = createWebcam();
			setPreferredSize(new Dimension(w, h));
			gameFrame = new GameFrame(this);
			gameFrame.addKeyListener(dataSender);
		}

		startGameLoop();
		if(!isKiPlayer) {
			Thread imageSender = new Thread(() -> {
				while (true) {
					dataSender.sendImage();
					try {
						Thread.sleep(10);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			});
			imageSender.start();
		}
	}

	public Game(SpectatorListener spectatorListener, SelectionWindow selectionWindow) {
		this.spectatorListener = spectatorListener;
		this.spectatorListener.setGame(this);
		this.selectionWindow = selectionWindow;

		setPreferredSize(new Dimension(w,h));
		gameFrame = new GameFrame(this);
		startGameLoop();
	}

	@Override
	public void paintComponent(final Graphics g) {
		super.paintComponent(g);

		if (imageFrame != null) {
			g.drawImage(imageFrame, 0, 0, w, h, null);
			lastImageFrame = imageFrame;
		} else if(lastImageFrame != null){
			g.drawImage(lastImageFrame, 0, 0, w, h, null);
		} else {
			g.setColor(backgroundColor);
			g.fillRect(0,0,w,h);
		}

		int yLine = 0;
		while (yLine < getHeight()) {
			g.setColor(foregroundColor);
			g.drawLine(getWidth()/2, yLine, getWidth()/2, yLine + 10);
			yLine += 20;
		}

		try {
			hostPlayer.draw(g);
			clientPlayer.draw(g);
			pong.draw(g);
			g.setColor(foregroundColor);
			g.drawString(String.valueOf(hostPlayer.score), getWidth()/2 - 40, 20);
			g.drawString(String.valueOf(clientPlayer.score), getWidth()/2 + 40, 20);
		} catch (NullPointerException e) {
			//e.printStackTrace();
		}
	}

	public void update() {
		pong.update();
		pong.update();

		Direction wallHit = pong.checkCollisionWorld();
		if (wallHit != null) {
			double phi = 0;
			if (wallHit == Direction.LEFT) {
				clientPlayer.score++;
				phi = pong.getRandomRightAngle();
			} else if (wallHit == Direction.RIGHT) {
				hostPlayer.score++;
				phi = pong.getRandomLeftAngle();
			}
			resetGame(phi);
		}

		pong.checkCollisionPlayer(hostPlayer);
		pong.checkCollisionPlayer(clientPlayer);
		hostPlayer.update();
		clientPlayer.update();
  	}

	private void startGameLoop() {
		gameLoop = new GameLoop(this);
		gameLoop.start();
	}

	private void resetGame(double angle) {
		clientPlayer.y = h/2-25;
		hostPlayer.y = h/2-25;
		pong.x = w/2;
		pong.y = h/2;
		pong.setVelocity(optionsPongSpeed, angle);
	}

	public void setHostPlayer(Player hostPlayer) {
		this.hostPlayer = hostPlayer;
	}

	public void setClientPlayer(Player clientPlayer) {
		this.clientPlayer = clientPlayer;
		this.clientPlayer.speed = optionsPlayerSpeed;
	}

	public void setPong(Pong pong) {
		this.pong = pong;
	}

	public void backToSelection() {
		gameFrame.dispose();
		gameLoop.stopGameLoop();
		if(server != null) {
			server.stopThreads();
		} else if(dataSender != null) {
			dataSender.stopListener();
		} else if(spectatorListener != null) {
			spectatorListener.stopListener();
		}
		selectionWindow.setVisible(true);
	}

	private Webcam createWebcam() {
		Webcam webcam = null;
		try {
			webcam = Webcam.getDefault();
			webcam.setViewSize(WebcamResolution.VGA.getSize());
			webcam.open();
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return webcam;
	}

	public byte[] getImage() {
		byte[] imageBuffer = null;
		if (webcam != null && webcam.isImageNew()) {
			try {
				imageBuffer = ImageConverter.getBytesFromImage(webcam.getImage());
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return imageBuffer;
	}

	public void setImage(byte[] imageBuffer) {
		try {
			imageFrame = ImageConverter.getImageFromBytes(imageBuffer);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}