import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.image.BufferedImage;
import java.io.*;
import java.net.*;
import java.nio.Buffer;

public class DataSender implements KeyListener {
	
    private ObjectOutputStream out;
    private ObjectInputStream in;
    private Game game;
    private byte[] serverIPByteArray;
    private String hostname;

    final static int MOVEMENT_UP = 0;
    final static int MOVEMENT_DOWN = 1;
    final static int MOVEMENT_STOP = 2;

	private boolean threadsActive = true;

    public DataSender(String serverIP, int port, int imagePort) {
    	try{
    		String[] splittedIP = serverIP.split(".");
			serverIPByteArray = new byte[] {
					(byte)Integer.parseInt(splittedIP[0]),
					(byte)Integer.parseInt(splittedIP[1]),
					(byte)Integer.parseInt(splittedIP[2]),
					(byte)Integer.parseInt(splittedIP[3]),
			};
		} catch (Exception e) {
			hostname = serverIP;
		}

        Socket socket;
        while(true) {
            try {
                socket = new Socket(serverIP, port);
                break;
            } catch (IOException e) {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e2) {
					e2.printStackTrace();
				}
            }
        }

        try {
            out = new ObjectOutputStream(socket.getOutputStream());
            in = new ObjectInputStream(socket.getInputStream());
        } catch (IOException e) {
            e.printStackTrace();
        }
        
        Thread t = new Thread(() -> {
			while(threadsActive) {
				listen();
			}
		});
		t.start();
    }

    public void setGame(Game game) {
        this.game = game;
    }

    public void sendObject(Object object) {
        try {
            out.writeObject(object);
            out.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void sendImage() {
		DatagramSocket clientSocket = null;
		try {
			clientSocket = new DatagramSocket();
			clientSocket.setSoTimeout(3000);
		} catch (SocketException ex) {
			ex.printStackTrace();
		}

		InetAddress IPAddress = null;
		try {
			if(serverIPByteArray != null) {
				IPAddress = InetAddress.getByAddress(serverIPByteArray);
			} else if(hostname != "") {
				IPAddress = InetAddress.getByName(hostname);
			} else {
				IPAddress = InetAddress.getLocalHost();
			}
		} catch (UnknownHostException ex) {
			ex.printStackTrace();
		}

		byte[] sendData = game.getImage();
		byte[] receiveData = new byte[100000];

		DatagramPacket sendPacket = new DatagramPacket(sendData, sendData.length, IPAddress, 8889);
		try {
			clientSocket.send(sendPacket);
		} catch (IOException ex) {
			ex.printStackTrace();
		}

		DatagramPacket receivePacket = new DatagramPacket(receiveData, receiveData.length);
		try {
			clientSocket.receive(receivePacket);
		} catch (IOException ex) {
			ex.printStackTrace();
		}

		game.setImage(receivePacket.getData());

		clientSocket.close();
	}
    
    private void listen() {
		try {
			Object data;
			while((data = in.readObject()) != null) {
				if(data.getClass() == Player.class) {
					Player player = (Player)data;
					if(player.isHost()) {
						game.setHostPlayer(player);
					} else {
					    game.setClientPlayer(player);
                    }
				} else if (data.getClass() == Pong.class) {
					game.setPong((Pong)data);
				} else if(data.getClass() == String.class) {
					if(data.equals("ClientAlreadyConnected")) {
						game.backToSelection();
					}
				} else if (data.getClass() == byte[].class) {
					game.setImage((byte[]) data);
				}
				if(!threadsActive) {
					break;
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void keyTyped(KeyEvent e) {}

	@Override
	public void keyPressed(KeyEvent e) {
		switch(e.getKeyCode()) {
		case KeyEvent.VK_UP:
			sendObject(MOVEMENT_UP);
			break;
		case KeyEvent.VK_DOWN:
			sendObject(MOVEMENT_DOWN);
			break;
		}
	}

	@Override
	public void keyReleased(KeyEvent e) {
		switch(e.getKeyCode()) {
			case KeyEvent.VK_UP:
			case KeyEvent.VK_DOWN:
				sendObject(MOVEMENT_STOP);
				break;
		}
	}

	public void stopListener() {
		threadsActive = false;
	}
}
