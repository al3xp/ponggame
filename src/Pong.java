import enums.Direction;
import util.MathUtil;

import java.awt.*;
import java.io.Serializable;

public class Pong implements Serializable{
	final static long serialVersionUID = 2;

	private double accelaration = 1.1;

	public double x,y,r,vx,vy;
	private Color color = Color.WHITE;
	
	public Pong(int x, int y, int r, double initVelocity) {
		this.x = x;
		this.y = y;
		this.r = r;
		double phi;
		if (Math.random() < 0.5) {
			phi = getRandomLeftAngle();
		} else {
			phi = getRandomRightAngle();
		}
		setVelocity(initVelocity, phi);
	}
	
	public void draw(Graphics g) {
		g.setColor(color);
		g.fillOval((int) Math.floor(x),(int) Math.floor(y),(int) Math.floor(r),(int) Math.floor(r));
	}

  	public double getRandomRightAngle() {
      return MathUtil.random(50, 130);
  	}

	public double getRandomLeftAngle() {
		return MathUtil.random(230, 310);
	}

	public void setVelocity(double velocity, double angle) {
		vx = (velocity * Math.cos(angle));
		vy = (velocity * Math.sin(angle));
	}

  public void update() {
	  	x += vx;
	  	y += vy;
  }

  public Direction checkCollisionWorld() {
	  if(y < 0 || y+r > Game.h) {
		  x -= vx;
		  y -= vy;
		  vy = -vy;
	  }

	  if(x < 0) {
		  return Direction.LEFT;
	  }
	  else if (x+r > Game.w) {
		  return Direction.RIGHT;
	  }
	return null;
  }

  public void checkCollisionPlayer(Player player) {
	  boolean isInYRange = player.y < y && (player.y + player.h) > y;

	  if (player.isHost()) {
		  if((player.x + player.w) > x && isInYRange) {
			  x -= vx;
			  y -= vy;
			  vx = -vx;
			  vx *= accelaration;
			  vy *= accelaration;
		  }
	  } else {
		  if((player.x) < x && isInYRange) {
			  x -= vx;
			  y -= vy;
			  vx = -vx;
			  vx *= accelaration;
			  vy *= accelaration;
		  }
	  }
  }
}
