import repositories.IpHistoryRepository;

import java.awt.*;
import java.util.Collections;
import java.util.Vector;

import javax.swing.*;

public class SelectionWindow extends JFrame {
	public SelectionWindow() {
		super("Pong");

		JButton buttonSinglePlayer = new JButton("SinglePlayer");
		JButton buttonHost = new JButton("Host");
		JButton buttonClient = new JButton("Client");
		JButton buttonSpectator = new JButton("Spectator");

		buttonSinglePlayer.addActionListener((event) -> {
			JLabel pongLabel = new JLabel("Pong Speed: 10");
			JLabel playerLabel = new JLabel("Player Speed: 10");
			JSlider pongSpeed = new JSlider(1,20,10);
			JSlider playerSpeed = new JSlider(1,20,10);
			pongSpeed.addChangeListener((changeEvent) -> {
				pongLabel.setText("Pong Speed: " + pongSpeed.getValue());
			});
			playerSpeed.addChangeListener((changeEvent) -> {
				playerLabel.setText("Player Speed: " + playerSpeed.getValue());
			});
			final JComponent[] inputs = new JComponent[] {
					pongLabel,
					pongSpeed,
					playerLabel,
					playerSpeed
			};
			int result = JOptionPane.showConfirmDialog(null, inputs, "Options", JOptionPane.PLAIN_MESSAGE);
			if (result == JOptionPane.OK_OPTION) {
				setVisible(false);
				new Game(new Server(8888, 8889), true, this, playerSpeed.getValue(), pongSpeed.getValue());
				new Game(new DataSender("localhost",8888, 8889), true, this);
			}
		});

		buttonHost.addActionListener((event) -> {
			JLabel pongLabel = new JLabel("Pong Speed: 10");
			JLabel playerLabel = new JLabel("Player Speed: 10");
			JSlider pongSpeed = new JSlider(1,20,10);
			JSlider playerSpeed = new JSlider(1,20,10);
			pongSpeed.addChangeListener((changeEvent) -> {
				pongLabel.setText("Pong Speed: " + pongSpeed.getValue());
			});
			playerSpeed.addChangeListener((changeEvent) -> {
				playerLabel.setText("Player Speed: " + playerSpeed.getValue());
			});
			final JComponent[] inputs = new JComponent[] {
					pongLabel,
					pongSpeed,
					playerLabel,
					playerSpeed
			};
			int result = JOptionPane.showConfirmDialog(null, inputs, "Options", JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE);
			if (result == JOptionPane.OK_OPTION) {
				setVisible(false);
				new Game(new Server(8888, 8889),false, this, playerSpeed.getValue(), pongSpeed.getValue());
			}
		});
		
		buttonClient.addActionListener((event) -> {
			JLabel ipInputLabel = new JLabel("Enter Host IP:");
			Vector<String> options = IpHistoryRepository.read();

			Vector<String> comboBoxOptions = (Vector<String>) options.clone();
			JButton clearHistoryButton = new JButton("Clear History");

			Collections.reverse(comboBoxOptions);
			JComboBox<String> ipInput = new JComboBox<>(comboBoxOptions);
			ipInput.setEditable(true);
			clearHistoryButton.addActionListener((e) -> {
				options.removeAllElements();
				ipInput.removeAllItems();
				IpHistoryRepository.write(options);
			});

			final JComponent[] inputs = new JComponent[] {
					ipInputLabel,
					ipInput,
					clearHistoryButton
			};

			int result = JOptionPane.showConfirmDialog(null, inputs, "Connect to Host", JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE);
			if (result == JOptionPane.OK_OPTION) {
				String serverIP = (String) ipInput.getSelectedItem();
				if (serverIP != null && !serverIP.isEmpty()) {
					serverIP.trim();
					System.out.println("ipInput: " + serverIP);

					options.remove(serverIP); // remove if already exists => no duplicates and newest on the bottom
					options.add(serverIP);

					IpHistoryRepository.write(options);

					setVisible(false);
					new Game(new DataSender(serverIP,8888, 8889),false, this);
				}
			} else if (result == JOptionPane.CANCEL_OPTION) {
				System.out.println("aborted");
			}

		});

		buttonSpectator.addActionListener((event) -> {
				String serverIP = (String) JOptionPane.showInputDialog(
						this,
						"Enter Host IP:",
						"Enter Host IP",
						JOptionPane.PLAIN_MESSAGE,
						null,
						null,
						"");
				serverIP = serverIP.trim();
				setVisible(false);
				new Game(new SpectatorListener(serverIP, 8888, 8889),this);
		});

		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		Container container = getContentPane();
		container.setLayout(new GridLayout(0,1));
		container.add(buttonSinglePlayer);
		container.add(buttonHost);
		container.add(buttonClient);
		container.add(buttonSpectator);
		
		setSize(300,300);
		setVisible(true);
	}
}
