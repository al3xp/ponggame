package repositories;

import java.io.*;
import java.util.Vector;

public class IpHistoryRepository {

	private static final File ipHistoryFile = new File(".ipHistory");


	public static Vector<String> read() {
		Vector<String> ipHistory = new Vector<>();
		if (ipHistoryFile.isFile()) {

			try (BufferedReader br = new BufferedReader(new FileReader(ipHistoryFile))) {
				String line;
				while ((line = br.readLine()) != null) {
					ipHistory.add(line);
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return ipHistory;
	}

	public static void write(Vector<String> ipHistory) {
		try {
			BufferedWriter writer = new BufferedWriter(new FileWriter(ipHistoryFile));

			StringBuilder sb = new StringBuilder();
			for (String ipAddress : ipHistory) {
				sb.append(ipAddress).append("\n");
			}
			writer.write(sb.toString());

			writer.close();

		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
